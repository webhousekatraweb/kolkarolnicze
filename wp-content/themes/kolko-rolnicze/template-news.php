<?php
/**
 * Template name: News
 *
 * Created by PhpStorm.
 * User: Szymon Katra
 * Date: 12.05.2016
 * Time: 20:00
 */

get_header();

$postCategory = get_post_meta( get_the_ID(), '_kolkarolnicze_post_category', true );
?>

<div class="container content template-news">
    <div class="row breadcrumbs">
        <div class="col-md-12">
            <?php echo get_post(9)->post_content; ?>
        </div>
    </div>
    <div class="row content-container">
        <?php get_sidebar(); ?>
        
        <div class="col-md-8 main-content">
            <?php
                if(!empty(get_post()->post_content)) {
                    echo get_post()->post_content;
                } else {
                    $args = array(
                        'posts_per_page'   => 1,
                        'offset'           => 0,
                        'category'         => '',
                        'category_name'    => $postCategory,
                        'orderby'          => 'date',
                        'order'            => 'DESC',
                        'post_type'        => 'post',
                        'post_status'      => 'publish',
                    );

                    $postek = get_posts($args)[0];

                    echo $postek->post_content;
                }
            ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
