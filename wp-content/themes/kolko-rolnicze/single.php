<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Kolko_Rolnicze
 */

get_header();

$postCategory = get_the_category()[0]->slug;

?>

<div class="container content single-post">
	<div class="row breadcrumbs">
		Agro News > <?php echo $post->post_title; ?>
	</div>
	<div class="row content-container">
		<?php get_sidebar(); ?>

		<div class="col-md-8 main-content">
			<?php echo $post->post_content; ?>
		</div>
	</div>
</div>


<?php
get_footer();
?>
