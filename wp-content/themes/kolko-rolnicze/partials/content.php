<?php
/**
 * Created by PhpStorm.
 * User: tomaszpach
 * Date: 10.05.2016
 * Time: 18:05
 */

?>

<div class="content container">
    <div class="article">
<!--        <div class="header col-md-12">-->
<!--            <span class="title"><h2>Komunikaty</h2></span>-->
<!--            <span class="title"><h2>Aktualności związkowe</h2></span>-->
<!--        </div>-->


        <div class="masonry">
            <div class="grid-sizer"></div>
            <?php
                $args = array(
                    'posts_per_page'   => -1,
                    'offset'           => 0,
                    'category'         => '',
                    'category_name'    => 'agro-news,rabaty',
                    'orderby'          => 'date',
                    'order'            => 'DESC',
                    'post_type'        => 'post',
                    'post_status'      => 'publish',
                );

                $postArray = get_posts($args);

            ?>
                <div class="stamp stamp1"></div>
            <?php
                foreach($postArray as $post) {
                    $postColor = get_post_meta( $post->ID, '_kolkarolnicze_post_setting_box_color', true );
                    $postColumns = get_post_meta( $post->ID, '_kolkarolnicze_masonry_post_columns', true );
            ?>
                    <div class="masonry-item <?php echo get_random_color_class(); ?>
                                <?php echo (!empty($postColumns) && $postColumns=='double')?' masonry-item--width2':''; ?>"
                         style="background-color: <?php echo (!empty($postColor))? $postColor : ''; ?>">

                        <p><?php echo $post->post_title; ?></p>

                    </div>
            <?php
                }
            ?>
        </div>

    </div>

</div>
