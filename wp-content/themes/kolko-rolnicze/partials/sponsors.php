<?php
/**
 * Created by PhpStorm.
 * User: Szymon Katra
 * Date: 11.06.2016
 * Time: 23:31
 */
?>

<div class="sponsors container">
    <?php
        for($i =0; $i<10; $i++){
    ?>
            <div class="sponsor">
                <div class="sponsorFlip">
                    <div class="img" style="background:url('<?php echo TEMPLATE_DIR_URI; ?>/assets/img/logo.png') no-repeat scroll center center / contain;"></div>
                </div>

                <div class="sponsorData">
                    <div class="sponsorDescription">
                        To jest zajebista firma
                    </div>
                    <div class="sponsorURL">
                        <a href="#">No wejdz tu</a>
                    </div>
                </div>
            </div>
    <?php
        }
    ?>
</div>