<?php
/**
 * Created by PhpStorm.
 * User: tomaszpach
 * Date: 10.05.2016
 * Time: 18:04
 */

?>

<div class="slider">
    <div class="slide">
        <div class="transparency">
            <div class="container">
                <div class="slide-title col-md-6">
                    <h1>XVIII Zjazd WZRKiOR w Krakowie</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="slide">
        <div class="transparency">
            <div class="container">
                <div class="slide-title col-md-6">
                    <h1>XI Regionalna Prezentacja Potraw i Wyrobów Wielkanocnych</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="slide">
        Test 3
    </div>
    <div class="slide">
        Test 4
    </div>
    <div class="slide">
        Test 5
    </div>
</div>
