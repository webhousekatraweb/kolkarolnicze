<?php
/**
 * Created by PhpStorm.
 * User: Szymon Katra
 * Date: 14.05.2016
 * Time: 17:28
 */


global $postCategory;
?>
<div class="col-md-4 sidebar-content">
	<?php
		if($postCategory == 'subpages') {
			$parent = array_reverse(get_post_ancestors(get_the_ID()));

			$args = array(
				'child_of' => ((!empty($parent[0]))? $parent[0] : get_the_ID()),
				'title_li' => null,
				'sort_order' => 'DESC',
				'sort_column' => 'post_date'
			);
	?>
		<ul class="links">
			<?php wp_list_pages($args); ?>
		</ul>
	<?php
		} else {
			$args = array(
				'posts_per_page'   => 10,
				'offset'           => 0,
				'category'         => '',
				'category_name'    => $postCategory,
				'orderby'          => 'date',
				'order'            => 'DESC',
				'post_type'        => 'post',
				'post_status'      => 'publish',
			);

			$posts_array = get_posts( $args );
	?>
			<ul class="links">
				<?php
				foreach($posts_array as $postek) {
					echo '<li><a href="'.$postek->guid.'">'.$postek->post_title.'</a></li>';
				}
				?>
			</ul>
	<?php
		}
	?>


	<ul class="button-links">
		<li><a href="#">Wydawnictwa</a></li>
		<li><a href="#">Video</a></li>
	</ul>
</div>
