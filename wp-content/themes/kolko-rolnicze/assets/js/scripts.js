/**
 * Created by Szymon Katra on 09.05.2016.
 */

jQuery(document).ready(function(){


    jQuery('.slider').slick({
        arrows: true,
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true
    });


// Header menu compress on load
    var $headerBanner = jQuery( 'header' );
    var previousScroll = 0;

    var scrollTop = jQuery(window).scrollTop();
    if( scrollTop > 10 ) {
        if( scrollTop >= 300 ) {
            $headerBanner.addClass( 'smaller' );
        } else {
            $headerBanner.removeClass( 'smaller' );
        }
    }

// Header menu on scroll animation

    jQuery(window).scroll( function() {
        var top = jQuery(window).scrollTop();

        if( top > 10 ) {

            if( top > previousScroll && top >= 300 ) {
                console.log($headerBanner);
                $headerBanner.addClass( 'smaller' );
            } else if( top < 300 ) {
                $headerBanner.removeClass( 'smaller' );
            }
            previousScroll = top;
        } else {
            $headerBanner.removeClass( 'smaller' );
        }
    } );

    jQuery('.masonry').masonry({
        // options
        itemSelector: '.masonry-item',
        columnWidth: '.grid-sizer',
        percentPosition: true,
        gutter: 10,
        transitionDuration: '0.8s',
        stamp: '.stamp'
    });


    jQuery('.sponsorFlip').bind("click",function(){

        // $(this) point to the clicked .sponsorFlip element (caching it in elem for speed):

        var elem = jQuery(this);

        // data('flipped') is a flag we set when we flip the element:

        if(elem.data('flipped'))
        {
            // If the element has already been flipped, use the revertFlip method
            // defined by the plug-in to revert to the default state automatically:

            elem.revertFlip();

            // Unsetting the flag:
            elem.data('flipped',false)
        }
        else
        {
            // Using the flip method defined by the plugin:

            elem.flip({
                direction:'lr',
                speed: 350,
                onBefore: function(){
                    // Insert the contents of the .sponsorData div (hidden from view with display:none)
                    // into the clicked .sponsorFlip div before the flipping animation starts:

                    elem.html(elem.siblings('.sponsorData').html());
                }
            });

            // Setting the flag:
            elem.data('flipped',true);
        }
    });
});
