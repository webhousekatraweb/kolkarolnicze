<?php
/**
 * Template Name: Home Page
 *
 */

get_header();

get_template_part('partials/slider', 'slider');

get_template_part('partials/content', 'content');

get_template_part('partials/sponsors', 'sponsors');

get_footer();
?>

