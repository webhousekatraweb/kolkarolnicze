<?php
/**
 * Kolko Rolnicze functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Kolko_Rolnicze
 */

/**
 * Defined constants
 */
define( 'TEMPLATE_DIR_PATH', get_template_directory() );
define( 'TEMPLATE_DIR_URI', get_template_directory_uri() );

/**
 * Theme styles (CSS)
 */
function theme_enqueue_style() {
		wp_enqueue_style( 'slick', TEMPLATE_DIR_URI . '/assets/css/extend/slick.css', array(), false );
		wp_enqueue_style( 'slick-theme', TEMPLATE_DIR_URI . '/assets/css/extend/slick-theme.css', array(), false );
		wp_enqueue_style( 'bootstrap', TEMPLATE_DIR_URI . '/assets/css/extend/bootstrap.min.css', array(), false );
		wp_enqueue_style( 'header', TEMPLATE_DIR_URI . '/assets/css/header.css', array(), false );
		wp_enqueue_style( 'footer', TEMPLATE_DIR_URI . '/assets/css/footer.css', array(), false );
		wp_enqueue_style( 'article', TEMPLATE_DIR_URI . '/assets/css/article.css', array(), false );
		wp_enqueue_style( 'main', TEMPLATE_DIR_URI . '/assets/css/main.css', array(), false );
		wp_enqueue_style( 'news', TEMPLATE_DIR_URI . '/assets/css/news.css', array(), false );
		wp_enqueue_style( 'masonry', TEMPLATE_DIR_URI . '/assets/css/partials/masonry.css', array(), false );
		wp_enqueue_style( 'sponsors', TEMPLATE_DIR_URI . '/assets/css/partials/sponsors.css', array(), false );
}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_style' );

/**
 * Theme styles (CSS) and scripts (JavaScript)
 */
function theme_enqueue_media() {
	wp_enqueue_script( 'slick', TEMPLATE_DIR_URI . '/assets/js/extend/slick.min.js', array(), false, true);
	wp_enqueue_script( 'bootstrap', TEMPLATE_DIR_URI . '/assets/js/extend/bootstrap.min.js', array(), false, true);
	wp_enqueue_script( 'scripts', TEMPLATE_DIR_URI . '/assets/js/scripts.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'masonry', TEMPLATE_DIR_URI . '/assets/js/masonry.pkgd.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'flip', TEMPLATE_DIR_URI . '/assets/js/jquery.flip.min.js', array( 'jquery' ), false, true );
}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_media' );

/**
 * Post thumbnails
 */

add_theme_support( 'post-thumbnails' );


/**
 * Register menus
 */

function register_my_menu() {
	register_nav_menu('header-menu',__( 'Header Menu' ));
	register_nav_menu('footer-menu',__( 'Footer Menu' ));
}
add_action( 'init', 'register_my_menu' );

/**
 * Add slider custom type
 */

add_action( 'init', 'create_slider_type', 1 );
function create_slider_type() {
	register_post_type( 'slider',
		array(
			'labels' => array(
				'name' => __( 'Sliders' ),
				'singular_name' => __( 'Slider' )
			),
			'public' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'publicly_queryable' => false,
			'supports'    => array( 'title', 'editor','thumbnail' )
		)
	);
}

// Add custom fields to pages for sidebar
add_action( 'cmb2_admin_init', 'cmb2_sidebar' );

function cmb2_sidebar() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_kolkarolnicze_';

	/**
	 * Initiate the metabox
	 */
	$cmb = new_cmb2_box( array(
		'id'            => 'post_category',
		'title'         => __( 'Post category', 'cmb2' ),
		'object_types'  => array( 'page', ), // Post type
		'context'       => 'normal', //normal, advanced or side
		'priority'      => 'core', // high,medium,low,core
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );

	$args = array(
		'orderby' => 'name',
		'parent' => 0,
		'hide_empty' => 0,
		'exclude' => '1',
	);
	$categories = get_categories( $args );
	$categoriesSelect = [];

	foreach($categories as $category) {
		$categoriesSelect[$category->slug] = $category->name;
	}

	$categoriesSelect['subpages'] = 'Subpages';

	$cmb->add_field( array(
		'name'             => 'Select post category to display',
		'desc'             => 'Select post category to display',
		'id'               => $prefix .'post_category',
		'type'             => 'select',
		'show_option_none' => true,
		'default'          => null,
		'options'          => $categoriesSelect,
	) );


	// Post custom fields
		$cmbPosts = new_cmb2_box( array(
			'id'            => 'post_settings',
			'title'         => __( 'Post settings', 'cmb2' ),
			'object_types'  => array( 'post', ), // Post type
			'context'       => 'normal', //normal, advanced or side
			'priority'      => 'core', // high,medium,low,core
			'show_names'    => true, // Show field names on the left
			// 'cmb_styles' => false, // false to disable the CMB stylesheet
			// 'closed'     => true, // Keep the metabox closed by default
		) );

		$cmbPosts->add_field( array(
			'name'    => 'Color of post box in home page',
			'id'      => $prefix .'post_setting_box_color',
			'type'    => 'colorpicker',
			'default' => null,
		) );

		$cmbPosts->add_field( array(
			'name'             => 'Column of post displayed in masonry plugin',
			'desc'             => 'Column of post displayed in masonry plugin',
			'id'               => $prefix .'masonry_post_columns',
			'type'             => 'select',
			'show_option_none' => true,
			'default'          => null,
			'options'          => [
				'single' => 'Single',
				'double' => 'Double'
			],
		) );
	//End post custom fields


}

//Remove text editor from sidebar page

//Rand masonry item color start
function get_random_color_class(){
	return 'color-'.rand(1,5);
}
//Rand masonry item color end