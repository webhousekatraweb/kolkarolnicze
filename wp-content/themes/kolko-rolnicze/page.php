<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Kolko_Rolnicze
 */

get_header();

$postCategory = get_post_meta( get_the_ID(), '_kolkarolnicze_post_category', true );
?>

	<div class="container content">
		<div class="row breadcrumbs">
			<div class="col-md-12">
				Agro News > Prezydent podpisał ustawę uniemożliwiającą do...
			</div>
		</div>
		<div class="row content-container">
			<?php get_sidebar(); ?>

			<div class="col-md-8 main-content">
				<?php
				if(!empty(get_post(get_the_ID())->post_content)) {
					echo get_post(get_the_ID())->post_content;
				} else {

					$args = array(
						'numberposts' => 1,
						'order' => 'DESC',
						'post_parent' => get_the_ID(),
						'post_type' => 'page',
					);

					$post_array = get_children($args);
					$postek = array_pop($post_array);

					echo ($postek) ? $postek->post_content : '';
				}
				?>
			</div>
		</div>
	</div>

<?php
get_footer();
